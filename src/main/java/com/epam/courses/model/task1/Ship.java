package com.epam.courses.model.task1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ship implements Serializable {

  private static final long serialVersionUID = 1L;

  private String shipModel;
  private Integer shipDroidCapacity;
  private List<Droid> droids = new ArrayList<>();

  public Ship() {
  }

  public Ship(String shipModel, Integer shipDroidCapacity) {
    this.shipModel = shipModel;
    this.shipDroidCapacity = shipDroidCapacity;
  }

  public void loadDroid(Droid droid) {
    droids.add(droid);
  }

  public String getShipModel() {
    return shipModel;
  }

  public void setShipModel(String shipModel) {
    this.shipModel = shipModel;
  }

  public Integer getShipDroidCapacity() {
    return shipDroidCapacity;
  }

  public void setShipDroidCapacity(Integer shipDroidCapacity) {
    this.shipDroidCapacity = shipDroidCapacity;
  }

  public List<Droid> getDroids() {
    return droids;
  }

  public void setDroids(List<Droid> droids) {
    this.droids = droids;
  }

  @Override
  public String toString() {
    return "Ship{" +
        "shipModel='" + shipModel + '\'' +
        ", shipDroidCapacity=" + shipDroidCapacity +
        ", droids=" + droids +
        '}';
  }
}
