package com.epam.courses.model.task5;

import com.epam.courses.controller.MainViewController;
import java.io.File;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileStructure {
  private static Logger log = LogManager.getLogger(FileStructure.class);

  private File file;

  public FileStructure(String fileName) {
    this.file = new File(fileName);
  }

  public FileStructure(File file) {
    this.file = file;
  }

  public void printDirectoryStructure(String tabulation) {
    if (file.isDirectory()) {
      log.info(tabulation + "Dir: " + file.getName());
      tabulation += "\t";
      File[] innerFiles = file.listFiles();
      for (File f : innerFiles) {
        new FileStructure(f).printDirectoryStructure(tabulation);
      }
    } else {
      log.info(tabulation+ "File: " + file.getName());
    }

  }

  public File getFile() {
    return file;
  }

  public void setFile(File file) {
    this.file = file;
  }
}
