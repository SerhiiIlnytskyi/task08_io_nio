package com.epam.courses.model.task3;

import java.io.IOException;
import java.io.InputStream;

public class CustomInputStream extends InputStream {

  private InputStream inputStream;
  private byte[] pushBackBuffer;
  private int currentPosition;

  public CustomInputStream(InputStream inputStream, int size) {
    this.inputStream = inputStream;
    if (size <= 0) {
      throw new IllegalArgumentException("size <= 0");
    }
    this.pushBackBuffer = new byte[size];
    this.currentPosition = size;
  }

  public CustomInputStream(InputStream in) {
    this(in, 1);
  }

  @Override
  public int read() throws IOException {
    if (currentPosition < pushBackBuffer.length) {
      return pushBackBuffer[currentPosition++] & 0xff;
    }
    return inputStream.read();
  }

  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    if (b == null) {
      throw new NullPointerException();
    } else if (off < 0 || len < 0 || len > b.length - off) {
      throw new IndexOutOfBoundsException();
    } else if (len == 0) {
      return 0;
    }

    int avail = pushBackBuffer.length - currentPosition;
    if (avail > 0) {
      if (len < avail) {
        avail = len;
      }
      System.arraycopy(pushBackBuffer, currentPosition, b, off, avail);
      currentPosition += avail;
      off += avail;
      len -= avail;
    }
    if (len > 0) {
      len = super.read(b, off, len);
      if (len == -1) {
        return avail == 0 ? -1 : avail;
      }
      return avail + len;
    }
    return avail;
  }

  @Override
  public int read(byte[] b) throws IOException {
    return this.read(b, 0, b.length);
  }

  public void unread(int b) throws IOException {
    if (currentPosition == 0) {
      throw new IOException("Push back buffer is full");
    }
    pushBackBuffer[--currentPosition] = (byte) b;
  }
}
