package com.epam.courses.model.task6;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class SomeBuffer {

  private static FileChannel fileChannelToRead;
  private static FileChannel fileChannelToWrite;
  private static ByteBuffer buffer;

  public SomeBuffer(String filePath) throws IOException {
    Path path = Paths.get(filePath);
    fileChannelToRead = FileChannel.open(path, StandardOpenOption.READ);
    fileChannelToWrite = FileChannel.open(path, StandardOpenOption.WRITE);
    buffer = ByteBuffer.allocate(1024);
  }

  public String readDataFromChannel() throws IOException {
    StringBuilder result = new StringBuilder();
    while (fileChannelToRead.read(buffer) != -1) {
      buffer.flip();
      while (buffer.hasRemaining()) {
        result.append((char) buffer.get());
      }
      buffer.clear();
    }
    return result.toString();
  }

  public void writeDataToChannel(String message) throws IOException {
    buffer = ByteBuffer.wrap(message.getBytes());
    fileChannelToWrite.write(buffer);
  }

  public void appendDataToChannel(String message) throws IOException{
    String dataFromFile = this.readDataFromChannel();
    this.writeDataToChannel(dataFromFile + message);
  }
}
