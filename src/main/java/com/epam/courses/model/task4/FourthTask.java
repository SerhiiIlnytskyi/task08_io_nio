package com.epam.courses.model.task4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FourthTask {

  private File file;

  public FourthTask(String filePath) {
    this.file = new File(filePath);
  }

  public List<String> getAllCommentsFromFile() throws IOException {
    List<String> comments = new ArrayList<>();
    try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
      while (bufferedReader.ready()) {
        StringBuilder comment = null;
        String line = bufferedReader.readLine().trim();
        if (line.startsWith("//")) {
          comment = new StringBuilder(line);
        }
        if (line.startsWith("/*")) {
          comment = new StringBuilder(line);
          while (!line.endsWith("*/")) {
            line = bufferedReader.readLine().trim();
            comment.append(line);
          }
        }
        if (comment != null) {
          comments.add(comment.toString());
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return comments;
  }
}
