package com.epam.courses.model.task2;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class SecondTask {
  private File file;

  public SecondTask(String filePath) {
    this.file = new File(filePath);
  }

  public int readWithoutBuffer() throws IOException {
    int byteSize = 0;
    InputStream inputStream = new FileInputStream(file);
    int data = inputStream.read();
    while (data != -1) {
      data = inputStream.read();
      byteSize++;
    }
    inputStream.close();
    return byteSize;
  }

  public int readWithBuffer(int bufferSize) throws IOException {
    int byteSize = 0;
    InputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file), bufferSize);
    int data = bufferedInputStream.read();
    while (data != -1) {
      data = bufferedInputStream.read();
      byteSize++;
    }
    bufferedInputStream.close();
    return byteSize;
  }

}
