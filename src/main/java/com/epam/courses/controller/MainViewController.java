package com.epam.courses.controller;

import com.epam.courses.model.task1.Droid;
import com.epam.courses.model.task1.FirstTask;
import com.epam.courses.model.task1.Ship;
import com.epam.courses.model.task2.SecondTask;
import com.epam.courses.model.task3.CustomInputStream;
import com.epam.courses.model.task4.FourthTask;
import com.epam.courses.model.task5.FileStructure;
import com.epam.courses.model.task6.SomeBuffer;
import com.epam.courses.model.task7.Client;
import com.epam.courses.model.task7.Server;
import java.io.IOException;
import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class MainViewController {

  private static Logger log = LogManager.getLogger(MainViewController.class);

  private FirstTask firstTask;
  private SecondTask secondTask;
  private CustomInputStream customInputStream;
  private FourthTask fourthTask;
  private FileStructure fileStructure;
  private SomeBuffer someBuffer;
  private Server server;
  private Client client;
  public MainViewController() {
    firstTask = new FirstTask("task1.txt");
    secondTask = new SecondTask("task2.pdf");
    customInputStream = new CustomInputStream(System.in);
    fourthTask = new FourthTask("task4.java");
    fileStructure = new FileStructure("C:\\Program Files\\Java\\jdk1.8.0_201");
    try {
      someBuffer = new SomeBuffer("task7.txt");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void testFirstTask() {
    log.info("First task test started....");
    Droid droid1 = new Droid("R2", 5, "worked");
    Droid droid2 = new Droid("R3", 6, "not worked");
    Droid droid3 = new Droid("Gemini", 15);
    Droid droid4 = new Droid("Imperial sentry droid", 100, "With breakage");
    Ship ship = new Ship();
    ship.setDroids(Arrays.asList(droid1, droid2, droid3, droid4));

    log.info("Object before serialization: " + ship.toString());
    firstTask.serializeObjects(ship);
    Ship deserializedShip = (Ship) firstTask.deserializeObjects();
    log.info("Object after deserialization: " + deserializedShip.toString());
    log.info("First task test finished.");
  }

  public void testSecondTask() {
    log.info("Second task test started....");
    try {
      long before = System.nanoTime();
      int bufferSize = 1 * 1024; //Buffer size = 1Kb
      int readedSize = secondTask.readWithBuffer(bufferSize);
      long after = System.nanoTime();

      log.info(String
          .format("Read with buffer: buffer size - %d bytes, file size - %d bytes. Spent time: %d ms",
              bufferSize, readedSize, after - before));

      before = System.nanoTime();
      bufferSize = 1 * 1024 * 1024; //Buffer size = 1Mb
      readedSize = secondTask.readWithBuffer(bufferSize);
      after = System.nanoTime();

      log.info(String
          .format("Read with buffer: buffer size - %d bytes, file size - %d bytes. Spent time: %d ms",
              bufferSize, readedSize, after - before));

      before = System.nanoTime();
      readedSize = secondTask.readWithoutBuffer();
      after = System.nanoTime();
      log.info(String
          .format("Read without buffer:  file size - %d bytes. Spent time: %d ms",
              readedSize, after - before));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void testThirdTask() {
    log.info("Third task test started....");
    try {
      log.info("Please input some symbol: ");
      int readByte = customInputStream.read();
      log.info(String.format("Read byte: %d", readByte));
      customInputStream.unread(readByte);
      log.info("Read byte pushed back");
      readByte = customInputStream.read();
      log.info(String.format("Read second byte: %d", readByte));
    } catch (IOException e) {
      e.printStackTrace();
    }
    log.info("Third task test finished.");
  }

  public void testFourthTask() {
    log.info("Fourth task test started....");
    try {
      fourthTask.getAllCommentsFromFile()
          .forEach(comment -> log.info(comment));
    } catch (IOException e) {
      e.printStackTrace();
    }
    log.info("Fourth task test finished.");
  }

  public void testFifthTask() {
    log.info("Fifth task test started....");

    fileStructure.printDirectoryStructure("");
    log.info("Fifth task test finished.");
  }

  public void testSixthTask() {
    log.info("Sixth task test started....");
    try {
      System.out.println(someBuffer.readDataFromChannel());
      someBuffer.writeDataToChannel("Some text to write ");
      someBuffer.appendDataToChannel("\nAppended0 ");
      someBuffer.appendDataToChannel("\nAppended1 ");
      someBuffer.appendDataToChannel("\nAppended2 ");
      log.info(someBuffer.readDataFromChannel());
    } catch (IOException e) {
      e.printStackTrace();
    }
    log.info("Sixth task test finished. ");
  }

  public void testSeventhTask() {

    server = new Server();
    try {
      server.start();
    } catch (IOException e) {
      e.printStackTrace();
    }
    client = new Client();
    log.info("Seventh task test started....");
    client.sendMessage("some message1");
    log.info("Seventh task test finished.");
  }
}
