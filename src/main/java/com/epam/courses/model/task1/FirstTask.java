package com.epam.courses.model.task1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FirstTask {

  private File file;

  public FirstTask(String fileName) {
    this.file = new File(fileName);
  }

  public void serializeObjects(Object object) {
    try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
        new FileOutputStream(file))) {
      objectOutputStream.writeObject(object);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public Object deserializeObjects() {
    try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
      return objectInputStream.readObject();
    } catch (ClassNotFoundException | IOException e) {
      e.printStackTrace();
    }
    return null;
  }
}
