package com.epam.courses.model.task1;

import java.io.Serializable;

public class Droid implements Serializable {
  private static final long serialVersionUID = 1L;

  private String droidModel;
  private Integer droidPower;
  private transient String droidStatus = "no status";

  public Droid() {
  }

  public Droid(String droidModel, Integer droidPower, String droidStatus) {
    this.droidModel = droidModel;
    this.droidPower = droidPower;
    this.droidStatus = droidStatus;
  }

  public Droid(String droidModel, Integer droidPower) {
    this.droidModel = droidModel;
    this.droidPower = droidPower;
  }

  public String getDroidModel() {
    return droidModel;
  }

  public void setDroidModel(String droidModel) {
    this.droidModel = droidModel;
  }

  public Integer getDroidPower() {
    return droidPower;
  }

  public void setDroidPower(Integer droidPower) {
    this.droidPower = droidPower;
  }

  public String getDroidStatus() {
    return droidStatus;
  }

  public void setDroidStatus(String droidStatus) {
    this.droidStatus = droidStatus;
  }

  @Override
  public String toString() {
    return "Droid{" +
        "droidModel='" + droidModel + '\'' +
        ", droidPower=" + droidPower +
        ", droidStatus='" + droidStatus + '\'' +
        '}';
  }
}
