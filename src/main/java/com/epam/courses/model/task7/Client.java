package com.epam.courses.model.task7;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Client {

  private static Logger log = LogManager.getLogger(Client.class);

  private SocketChannel client;
  private ByteBuffer buffer;

  public Client() {
    try {
      InetSocketAddress inetSocketAddress = new InetSocketAddress(InetAddress.getLocalHost(), 7575);
      client = SocketChannel.open(inetSocketAddress);
      buffer = ByteBuffer.allocate(256);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void stop() throws IOException {
    client.close();
    buffer = null;
  }

  public String sendMessage(String message) {
    buffer = ByteBuffer.wrap(message.getBytes());
    String responseString = null;
    try {
      client.write(buffer);
      buffer.clear();
      client.read(buffer);
      responseString = new String(buffer.array()).trim();
      log.info(responseString);
      buffer.clear();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return responseString;
  }

  public SocketChannel getClient() {
    return client;
  }

  public void setClient(SocketChannel client) {
    this.client = client;
  }

  public static void main(String[] args) {
    Client client = new Client();
    client.sendMessage("some message1");
  }
}
