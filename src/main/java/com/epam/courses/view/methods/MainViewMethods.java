package com.epam.courses.view.methods;

import com.epam.courses.controller.MainViewController;
import com.epam.courses.view.View;
import java.util.LinkedHashMap;
import java.util.Map;

public class MainViewMethods implements ViewMethods {

  private MainViewController mainViewController;
  private Map<String, Executable> methodsMenu;

  public MainViewMethods() {
    this.mainViewController = new MainViewController();
    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::button1);
    methodsMenu.put("2", this::button2);
    methodsMenu.put("3", this::button3);
    methodsMenu.put("4", this::button4);
    methodsMenu.put("5", this::button5);
    methodsMenu.put("6", this::button6);
    methodsMenu.put("7", this::button7);
    methodsMenu.put("L", this::languageMenu);
  }

  private void button1() {
    mainViewController.testFirstTask();
  }

  private void button2() {
    mainViewController.testSecondTask();
  }

  private void button3() {
    mainViewController.testThirdTask();
  }

  private void button4() {
    mainViewController.testFourthTask();
  }

  private void button5() {
    mainViewController.testFifthTask();
  }

  private void button6() {
    mainViewController.testSixthTask();
  }

  private void button7() {
    mainViewController.testSeventhTask();
  }

  private void languageMenu() {
    new View("LanguageView").show();
  }

  public Map<String, Executable> getMethodsMenu() {
    return methodsMenu;
  }

  public void setMethodsMenu(Map<String, Executable> methodsMenu) {
    this.methodsMenu = methodsMenu;
  }

}
